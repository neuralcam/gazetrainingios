//
//  FaceDetection.swift
//  GazeTrainingData
//
//  Created by Botond Bocsi on 5/18/18.
//  Copyright © 2018 Botond Bocsi. All rights reserved.
//

import Foundation
import UIKit
import Vision
import CoreMotion

class FaceDetection {
    
    ////////////////////////////////
    // Public
    ////////////////////////////////
    
    // Returns face features from image in the following order:
    // origna image, face, left eye, right eye, merged/resized image
    public func detecFaceFeatures(face: VNFaceObservation, originalImage: UIImage) -> [UIImage] {
        
        // face bounding box
        let w = face.boundingBox.size.width * originalImage.size.width
        let h = face.boundingBox.size.height * originalImage.size.height
        let x = face.boundingBox.origin.x * originalImage.size.width
        let y = face.boundingBox.origin.y * originalImage.size.height
        var faceRect = CGRect(x: x, y: y, width: w, height: h)
        
        // left eye
        var left_corner_x = Float(10000.0)
        var left_corner_y = Float(-10000.0)
        var left_center_x = Float(0.0)
        var left_center_y = Float(0.0)
        if let landmark = face.landmarks?.leftEye {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if Float(point.x) < left_corner_x {
                    left_corner_x = Float(point.x)
                }
                if Float(point.y) > left_corner_y {
                    left_corner_y = Float(point.y)
                }
                left_center_x += Float(point.x)
                left_center_y += Float(point.y)
            }
            left_center_x /= Float(landmark.pointCount)
            left_center_y /= Float(landmark.pointCount)
        }
        
        // right eye
        var right_corner_x = Float(-10000.0)
        var right_corner_y = Float(-10000.0)
        var right_center_x = Float(0.0)
        var right_center_y = Float(0.0)
        if let landmark = face.landmarks?.rightEye {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if Float(point.x) > right_corner_x {
                    right_corner_x = Float(point.x)
                }
                if Float(point.y) > right_corner_y {
                    right_corner_y = Float(point.y)
                }
                right_center_x += Float(point.x)
                right_center_y += Float(point.y)
            }
            right_center_x /= Float(landmark.pointCount)
            right_center_y /= Float(landmark.pointCount)
        }
    
        // left eyebrow
        if let landmark = face.landmarks?.leftEyebrow {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if Float(point.x) < left_corner_x {
                    left_corner_x = Float(point.x)
                }
                if Float(point.y) > left_corner_y {
                    left_corner_y = Float(point.y)
                }
            }
        }
        
        // right eyebrow
        if let landmark = face.landmarks?.rightEyebrow {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if Float(point.x) > right_corner_x {
                    right_corner_x = Float(point.x)
                }
                if Float(point.y) > right_corner_y {
                    right_corner_y = Float(point.y)
                }
            }
        }
        
        let shift_px = CGFloat(10)
        // left eye boundig box
        let w1 = CGFloat(2*(left_corner_y - left_center_y)) * h + shift_px * 2
        let x1 = CGFloat(left_corner_x) * w - shift_px
        let y1 = CGFloat(2*left_center_y - left_corner_y) * h - shift_px
        var faceRect1 = CGRect(x: x + x1, y: y + y1, width: w1, height: w1)
        
        // right eye boundig box
        let w2 = CGFloat(2*(left_corner_y - left_center_y)) * h + shift_px * 2
        let x2 = CGFloat(2*right_center_x - right_corner_x) * w - shift_px
        let y2 = CGFloat(2*right_center_y - right_corner_y) * h - shift_px
        var faceRect2 = CGRect(x: x + x2, y: y + y2, width: w2, height: w2)
        
        let finalImage = originalImage
        faceRect.origin.y = originalImage.size.height - faceRect.origin.y - faceRect.size.height
        faceRect1.origin.y = originalImage.size.height - faceRect1.origin.y - faceRect1.size.height
        faceRect2.origin.y = originalImage.size.height - faceRect2.origin.y - faceRect2.size.height
        
        let finalFace = originalImage.cropToRect(rect: faceRect)!
        let leftEye = originalImage.cropToRect(rect: faceRect1)!
        let rightEye = originalImage.cropToRect(rect: faceRect2)!
        
        return [finalImage, finalFace, leftEye, rightEye]
    }
    
    ////////////////////////////////
    // Private
    ////////////////////////////////
    
    private func resizedImage(image: UIImage, newSize: CGSize) -> UIImage {
        // Defines image size
        let imageSize = newSize
        // initialize image canvas
        UIGraphicsBeginImageContext(imageSize)
        // resizes image
        image.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        // get reduced image
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        // end canvas
        UIGraphicsEndImageContext()
        // return small image (UIImage)
        return smallImage!
    }
}
