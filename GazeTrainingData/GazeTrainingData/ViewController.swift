//
//  ViewController.swift
//  GazeTrainingData
//
//  Created by Botond Bocsi on 5/18/18.
//  Copyright © 2018 Botond Bocsi. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import MessageUI
import CoreMotion


////////////////////
// Main Class
////////////////////
class ViewController: UIViewController,AVCaptureVideoDataOutputSampleBufferDelegate {

    ////////////////////////////////
    // Config
    ////////////////////////////////

    let _experimentLengthTime = 2*60*1000 // Length of the experiment after what the data will be send to the cloud
    let _dotUpdateTime = 3000 // How often to update the dot position
    let _eyeTransistionTime = 500 // How much time to wait until start recording eye position
    let _frameTransitionTime = 500 // Minimum time between two frames being capture
    
    static let _widthCount = 4
    static let _heightCount = 7
    
    ////////////////////////////////
    // Data
    ////////////////////////////////
    
    let _faceDetection:FaceDetection = FaceDetection()
    let _dataUploader:DataUploader = DataUploader()
    
    /*
    let _dotGeneration:DotGeneration = DotGeneration(
        width: 750, height:1334,
        cmWidth: 5.8, cmHeight: 10.4,
        cameraShiftX: 1.8, cameraShiftY: 0.8,
        widthCount: _widthCount, heightCount: _heightCount)
    */
    
    
    let _dotGeneration:DotGeneration = DotGeneration(
        width: Int(UIScreen.main.bounds.width) * 2, height: Int(UIScreen.main.bounds.height) * 2,
        cmWidth: 0.0, cmHeight: 0.0,
        cameraShiftX: 0.0, cameraShiftY: 0.0,
        widthCount: _widthCount, heightCount: _heightCount)
 
    let _label: UILabel = {
        let _label = UILabel()
        _label.textColor = .blue
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.text = "O"
        _label.font = UIFont.boldSystemFont(ofSize: 30.0)
        return _label
    }()
    
    var _currentDot = Dot(0, 0, 0.0, 0.0, 0)
    var _image: UIImage!
    var _dotUpdateTimeMs:UInt64 = 0
    var _frameRecordTimeMs:UInt64 = 0
    var _experimentStartTimeMs:UInt64 = 0
    
    let _shapeLayer = CAShapeLayer()
    let _context = CIContext()
    let _motionManager = CMMotionManager()
    var _dotUpdatertimer:Timer?
    
    ////////////////////////////////
    // Init
    ////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        _motionManager.startDeviceMotionUpdates();
        _motionManager.startAccelerometerUpdates();
        
        setupCaptureSession()
        
        // Setup UI
        view.addSubview(_label)
        _label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        
        _experimentStartTimeMs = DispatchTime.now().uptimeNanoseconds / 1_000_000
        updateDot()
        _dotUpdatertimer = Timer.scheduledTimer(timeInterval: Double(_dotUpdateTime / 1000), target: self, selector: #selector(ViewController.updateDot), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////////////////////////////////
    // Private
    ////////////////////////////////
    
    @objc func updateDot() {
        _currentDot = _dotGeneration.getRandomDot()
        
        DispatchQueue.main.async { // Correct
            self._label.center  = CGPoint(x: self._currentDot.X/2, y: self._currentDot.Y/2)
            self._dotUpdateTimeMs = DispatchTime.now().uptimeNanoseconds / 1_000_000
        }
    }
    
    func setupCaptureSession() {
        let captureSession = AVCaptureSession()
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .front).devices
        do {
            if let captureDevice = availableDevices.first {
                let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
                captureSession.addInput(captureDeviceInput)
            }
        } catch {
            print(error.localizedDescription)
        }
        let captureOutput = AVCaptureVideoDataOutput()
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        captureSession.addOutput(captureOutput)
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.frame
        view.layer.addSublayer(previewLayer)
        view.layer.addSublayer(_shapeLayer)
        captureSession.startRunning()
    }

    func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        guard let cgImage = _context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }
    
    @objc func imageSelector(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        let nowTime = DispatchTime.now().uptimeNanoseconds / 1_000_000
        
        // Check if experiment should be stopped
        if nowTime - self._experimentStartTimeMs > UInt64(self._experimentLengthTime) {
            if _dotUpdatertimer != nil {
                _dotUpdatertimer?.invalidate()
                _dotUpdatertimer = nil
            }
            DispatchQueue.main.async { // Correct
                self._label.textColor = .red
                self._label.center  = CGPoint(x: 100, y: 20)
                self._label.text = "Uploading... Do not close app!!"
            }
            _dataUploader.uploadDataToCloud()
        }
        
        // Check if enough time has passed since dot update
        if nowTime - self._dotUpdateTimeMs < UInt64(self._eyeTransistionTime) {
            return;
        }
        
        // Check if enough time has passed since last frame recording
        if nowTime - _frameRecordTimeMs < UInt64(self._frameTransitionTime) {
            return;
        }
        _frameRecordTimeMs = nowTime
        
        // Get image and process face recognition
        _image = imageFromSampleBuffer(sampleBuffer: sampleBuffer)
        _image = UIImage(cgImage: _image.cgImage!, scale: _image.scale, orientation: UIImageOrientation.right)
        let faceLandmarksRequest = VNDetectFaceLandmarksRequest(completionHandler: self.handleFaceFeatures)
        let requestHandler = VNImageRequestHandler(cgImage: _image.cgImage!, orientation: CGImagePropertyOrientation.right ,options: [:])
        do {
            try requestHandler.perform([faceLandmarksRequest])
        } catch {
            print(error)
        }
    }
    
    func collectSensorInfo() -> [Double] {
        var sensorData: [Double] = []
        
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m11)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m12)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m13)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m21)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m22)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m23)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m31)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m31)
        sensorData.append(_motionManager.deviceMotion!.attitude.rotationMatrix.m33)
        
        sensorData.append(_motionManager.deviceMotion!.attitude.pitch)
        sensorData.append(_motionManager.deviceMotion!.attitude.roll)
        sensorData.append(_motionManager.deviceMotion!.attitude.yaw)
        
        sensorData.append(_motionManager.deviceMotion!.attitude.quaternion.x)
        sensorData.append(_motionManager.deviceMotion!.attitude.quaternion.y)
        sensorData.append(_motionManager.deviceMotion!.attitude.quaternion.z)
        sensorData.append(_motionManager.deviceMotion!.attitude.quaternion.w)
        
        return sensorData
    }
    
    func handleFaceFeatures(request: VNRequest, errror: Error?) {
        guard let observations = request.results as? [VNFaceObservation] else {
            fatalError("unexpected result type!")
        }
        for face in observations {
            UIGraphicsBeginImageContextWithOptions(_image.size, true, 0.0)
            let context = UIGraphicsGetCurrentContext()
            _image.draw(in: CGRect(x: 0, y: 0, width: _image.size.width, height: _image.size.height))
            context?.translateBy(x: 0, y: _image.size.height)
            context?.scaleBy(x: 1.0, y: -1.0)
            let img = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            // Detect face
            let faceFeatures = _faceDetection.detecFaceFeatures(face: face, originalImage: img)
         
            // Collect sensor information
            let sensorData = collectSensorInfo()
            
            // Save Data
            _dataUploader.saveAllDataToFile(faceFeatures: faceFeatures, sensorData: sensorData, dot: self._currentDot)
            
            print("Data saved with id: " + String(_dataUploader.getDataCount()))
            return // Only one observation is needed
        }
    }
    
    ////////////////////////////////
    // Interface
    ////////////////////////////////
    

}

