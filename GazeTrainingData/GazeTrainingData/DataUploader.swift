//
//  DataUploader.swift
//  GazeTrainingData
//
//  Created by Botond Bocsi on 5/21/18.
//  Copyright © 2018 Botond Bocsi. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import MessageUI
import CoreMotion
import AWSS3
import AWSCore

class DataUploader {
    
    ////////////////////////////////
    // Config
    ////////////////////////////////
    
    let _accessKey = "AKIAJUXLJMFWQFSWFSVA"
    let _secretKey = "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"
    let _bucketName = "bboti-gaze-training"
    let _folderPrefix = "happy_friday/"
    
    ////////////////////////////////
    // Data
    ////////////////////////////////
    
    private var _savedData:[[String]] = []
    private var _savedURLs:[[URL]] = []
    private var _frameId:Int32 = 0
    private var _experimentId:UInt64 = 0
    private var _hasBeenUploaded = false
    private var _groupUploads = DispatchGroup()
    
    ////////////////////////////////
    // Init
    ////////////////////////////////
    
    init () {
        _experimentId = UInt64(floor(NSDate().timeIntervalSince1970 * 1000))
    }
    
    ////////////////////////////////
    // Public
    ////////////////////////////////
    
    func saveAllDataToFile(faceFeatures:[UIImage], sensorData:[Double], dot:Dot) {
        
        OSAtomicIncrement32(&self._frameId)
        
        let originalImageName = "img_original_" + String(self._experimentId) + "_" + String(self._frameId) + ".png"
        let faceImageName = "img_face_" + String(self._experimentId) + "_" + String(self._frameId) + ".png"
        let leftImageName = "img_left_" + String(self._experimentId) + "_" + String(self._frameId) + ".png"
        let rightImageName = "img_right_" + String(self._experimentId) + "_" + String(self._frameId) + ".png"
        let sensorFilename = "sensor_" + String(self._experimentId) + "_" + String(self._frameId) + ".txt"
        let labelFilename = "label_" + String(self._experimentId) + "_" + String(self._frameId) + ".txt"
        
        let urlOriginal = saveImage(image: faceFeatures[0], imageName: originalImageName)
        let urlFace = saveImage(image: faceFeatures[1], imageName: faceImageName)
        let urlLeftEye = saveImage(image: faceFeatures[2], imageName: leftImageName)
        let urlRightEye = saveImage(image: faceFeatures[3], imageName: rightImageName)
        let urlSensors = saveSensorData(sensorData: sensorData, fileName: sensorFilename)
        let urlLabel = saveLabelData(dot: dot, fileName: labelFilename)
        
        var thisFrame:[String] = []
        thisFrame.append(originalImageName)
        thisFrame.append(faceImageName)
        thisFrame.append(leftImageName)
        thisFrame.append(rightImageName)
        thisFrame.append(sensorFilename)
        thisFrame.append(labelFilename)
        _savedData.append(thisFrame)
        
        var thisUrl:[URL] = []
        thisUrl.append(urlOriginal!)
        thisUrl.append(urlFace!)
        thisUrl.append(urlLeftEye!)
        thisUrl.append(urlRightEye!)
        thisUrl.append(urlSensors!)
        thisUrl.append(urlLabel!)
        _savedURLs.append(thisUrl)
    }
    
    func uploadDataToCloud() {
        
        // Check if we have already uploaded data
        if _hasBeenUploaded {
            return
        }
        _hasBeenUploaded = true
        
        let credentialProvider = AWSStaticCredentialsProvider(accessKey: self._accessKey, secretKey: self._secretKey)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.EUCentral1, credentialsProvider: credentialProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let transferManager = AWSS3TransferManager.default()
        for i in 0...self._savedData.count-1 {
            _groupUploads.enter()
            let uploadRequest0 = makeUploadRequest(frameIdx: i, featureIdx:0, type: "image/png")
            transferManager.upload(uploadRequest0).continueWith(executor:AWSExecutor.mainThread(), block:uploadContinueCallback )
            
            _groupUploads.enter()
            let uploadRequest1 = makeUploadRequest(frameIdx: i, featureIdx:1, type: "image/png")
            transferManager.upload(uploadRequest1).continueWith(executor:AWSExecutor.mainThread(), block:uploadContinueCallback )
            
            _groupUploads.enter()
            let uploadRequest2 = makeUploadRequest(frameIdx: i, featureIdx:2, type: "image/png")
            transferManager.upload(uploadRequest2).continueWith(executor:AWSExecutor.mainThread(), block:uploadContinueCallback )
            
            _groupUploads.enter()
            let uploadRequest3 = makeUploadRequest(frameIdx: i, featureIdx:3, type: "image/png")
            transferManager.upload(uploadRequest3).continueWith(executor:AWSExecutor.mainThread(), block:uploadContinueCallback )
            
            _groupUploads.enter()
            let uploadRequest4 = makeUploadRequest(frameIdx: i, featureIdx:4, type: "text/plain")
            transferManager.upload(uploadRequest4).continueWith(executor:AWSExecutor.mainThread(), block:uploadContinueCallback )
            
            _groupUploads.enter()
            let uploadRequest5 = makeUploadRequest(frameIdx: i, featureIdx:5, type: "text/plain")
            transferManager.upload(uploadRequest5).continueWith(executor:AWSExecutor.mainThread(), block:uploadContinueCallback )
        }
        
        self._groupUploads.notify(queue: .main) {
            print("All images uploaded")
            exit(0)
        }
    }
    
    func getDataCount() -> Int32 {
        return self._frameId
    }
    
    ////////////////////////////////
    // Private
    ////////////////////////////////
    
    private func makeUploadRequest(frameIdx: Int, featureIdx:Int, type:String) -> AWSS3TransferManagerUploadRequest {
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = self._savedURLs[frameIdx][featureIdx]
        uploadRequest.key = _folderPrefix + self._savedData[frameIdx][featureIdx]
        uploadRequest.bucket = _bucketName
        uploadRequest.contentType = type //"image/png"
        uploadRequest.acl = .publicRead
        return uploadRequest
    }
    
    private func uploadContinueCallback(task:AWSTask<AnyObject>) -> AnyObject? {
        if let error = task.error {
            print("Upload failed: (\(error.localizedDescription))")
        }
        self._groupUploads.leave()
        return nil
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func saveImage(image: UIImage, imageName: String) -> URL? {
        if let data = UIImagePNGRepresentation(image.resizeImage(newWidth: CGFloat(224))) {
            let filename = getDocumentsDirectory().appendingPathComponent(imageName)
            try? data.write(to: filename)
            return filename
        }
        return nil
    }
    
    private func saveSensorData(sensorData: [Double], fileName: String) -> URL? {
        var text:String = ""
        for i in 0...sensorData.count-1 {
            text += String(sensorData[i]) + "\n"
        }
        let filename = getDocumentsDirectory().appendingPathComponent(fileName)
        try? text.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        return filename
    }
    
    private func saveLabelData(dot:Dot, fileName: String) -> URL? {
        var text:String = ""
        text += String(dot.Class) + "\n"
        text += String(dot.X) + "\n"
        text += String(dot.Y) + "\n"
        text += String(dot.cm_X) + "\n"
        text += String(dot.cm_Y) + "\n"
        text += String(NSDate().timeIntervalSince1970) + "\n"
        text += UIDevice.modelName + "\n"
        text += String(Int(UIScreen.main.bounds.width) * 2) + "\n"
        text += String(Int(UIScreen.main.bounds.height) * 2) + "\n"
        let filename = getDocumentsDirectory().appendingPathComponent(fileName)
        try? text.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        return filename
    }
}
