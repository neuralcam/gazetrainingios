//
//  DotGeneration.swift
//  GazeTrainingData
//
//  Created by Botond Bocsi on 5/18/18.
//  Copyright © 2018 Botond Bocsi. All rights reserved.
//

import Foundation

////////////////////
// Dot Class
////////////////////
class Dot {
    
    public init(_ x:Int, _ y:Int, _ cm_x:Double, _ cm_y:Double, _ dotClass:Int) {
        self.X = x
        self.Y = y
        self.Class = dotClass
        self.cm_X = cm_x
        self.cm_Y = cm_y
    }
    
    public var Class:Int
    public var X:Int
    public var Y:Int
    public var cm_X:Double
    public var cm_Y:Double
}

////////////////////
// Dot Generation Class
////////////////////
class DotGeneration {
    
    ////////////////////////////////
    // Data
    ////////////////////////////////
    
    private var _camShiftX:Double
    private var _camShiftY:Double
    
    private var _widthCount:Int
    private var _heightCount:Int
    private var _xStepCm: Double
    private var _yStepCm: Double
    private var _xStepPx: Int
    private var _yStepPx: Int
    
    ////////////////////////////////
    // Init
    ////////////////////////////////
    
    init(
        width:Int,
        height:Int,
        cmWidth:Double,
        cmHeight:Double,
        cameraShiftX:Double,
        cameraShiftY:Double,
        widthCount:Int,
        heightCount:Int) {
        self._camShiftX = cameraShiftX
        self._camShiftY = cameraShiftY
        self._widthCount = widthCount
        self._heightCount = heightCount
        
        self._xStepCm = cmWidth / Double(widthCount)
        self._yStepCm = cmHeight / Double(heightCount)
        self._xStepPx = width / (widthCount)
        self._yStepPx = height / (heightCount)
    }
    
    ////////////////////////////////
    // Public
    ////////////////////////////////
    
    public func classToDot(classId:Int) -> Dot {
        let rX:Int = classId % _widthCount
        let rY:Int = classId / _widthCount
        
        return Dot(
            rX * _xStepPx + _xStepPx/2,
            rY * _yStepPx + _xStepPx/2,
            Double(rX) * _xStepCm - _camShiftX + _xStepCm/2,
            Double(rY) * _yStepCm + _camShiftY + _yStepCm/2,
            classId)
    }
    
    public func getRandomDot() -> Dot {
        
        let rndX = Int(arc4random_uniform(UInt32(_widthCount)))
        let rndY = Int(arc4random_uniform(UInt32(_heightCount)))
        
        return Dot(
            rndX * _xStepPx + _xStepPx/2,
            rndY * _yStepPx + _xStepPx/2,
            Double(rndX) * _xStepCm - _camShiftX + _xStepCm/2,
            Double(rndY) * _yStepCm + _camShiftY + _yStepCm/2,
            getDotClass(rndX, rndY))
    }
    
    public func getDotClassCount() -> Int {
        return _widthCount * _heightCount
    }
    
    private func getDotClass(_ x:Int, _ y:Int) -> Int {
        return x + y * _widthCount
    }
}








