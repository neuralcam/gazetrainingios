Gaze Your Phone for the Science!!

This README describes how you can help us to collect training data for our Gaze Tracking project. The project is about predicting where are you looking on you iPhone's screen based on camera images. Please do the following steps:

* Read this README until end

* Clone this project, build it, and install it on your iPhone

* Before starting the app, make sure you are in a comfortable, natural position in which you would use your phone. One experiments takes 2 minutes, you should not be distracted for this period.

* We will upload roughly 20 megabytes of data so make sure you have wifi enabled if you do not want to use the 3G/4G network.

* During data collection, a blue dot appears on the screen and changes its location at every 3 seconds. You should follow with you gaze this blue dot. That is all!! Note that no email, sms, or facebook notification should distract you!

* After the 2 minutes of the experiments the blue dot disappears and a red message appears instead. At this point DO NOT CLOSE THE APP, since data uploading is taking place. When data uploading is finished, the app is automatically closed.

* If for some reasons you lost tracking the blue dot with your eye, close the app immediately (kill the process), thus, the data uploading will not take place.

* You can repeat the experiment as many times as you want.

## How to distribute the app

To distribute this application through Fabric, you need [Fastlane](https://fastlane.tools) installed on your machine. Once you have it installed, just run the following command:

```bash
fastlane beta
```